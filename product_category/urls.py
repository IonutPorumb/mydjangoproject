from django.urls import path

from product_category import views

urlpatterns = [
    path('create-category-list/', views.CategoryListView.as_view(), name='create_category_list'),
    path('create-category/', views.CategoryCreateView.as_view(), name='show_category_list'),
    path('category-detail-view/<int:pk>/', views.CategoryDetailView.as_view(), name='detail_category_list'),
    path('edit-category-in-list/<int:pk>/', views.CategoryUpdateView.as_view(), name='edit_category_in_list'),
    path('delete-category-in-list/<int:pk>/', views.CategoryDeleteView.as_view(), name='delete_category'),

]
