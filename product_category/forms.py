from django import forms
from django.forms import TextInput, Select

from product_category.models import Category


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['application_area', 'supply_voltage', 'product_family', 'performance']

        widgets = {
            'application_area': TextInput(attrs={'placeholder': 'Please insert the application area',
                                                 'class': 'form-control'}),
            'supply_voltage': Select(attrs={'class': 'form-control'}),
            'product_family': TextInput(attrs={'placeholder': 'Please insert the product family',
                                               'class': 'form-control'}),
            'performance': TextInput(attrs={'placeholder': 'Please insert the product performance',
                                            'class': 'form-control'}),

        }

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
