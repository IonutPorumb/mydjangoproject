from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, DeleteView, UpdateView

from product_category.forms import CategoryForm

from product_category.models import Category


# Create products list page
class CategoryListView(LoginRequiredMixin, PermissionRequiredMixin,ListView):
    template_name = 'product_category/list_category.html'
    model = Category
    context_object_name = 'all_categories'


# Create category detail view
class CategoryDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'product_category/detail_category.html'
    permission_required = 'product_category.detail_category'
    model = Category
    context_object_name = 'category_detail'


# Create products in DB
class CategoryCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'product_category/create_category.html'
    permission_required = 'product_category.create_category'
    form_class = CategoryForm
    success_url = reverse_lazy('create_category')


# Edit created products
class CategoryUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'product_category/update_category_list.html'
    permission_required = 'product_category.update_category_list'
    model = Category
    form_class = CategoryForm
    success_url = reverse_lazy('show_category_list')


# Delete existing products
class CategoryDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'product_category/delete_category.html'
    permission_required = 'product_category.delete_category'
    model = Category
    success_url = reverse_lazy('show_category_list')
    context_object_name = 'categoryList'
