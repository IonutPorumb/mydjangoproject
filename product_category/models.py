from django.db import models


# Create your models here.
class Category(models.Model):
    voltage = (('24', '24'), ('230', '230'), ('380', '380'), ('400', '400'), ('690', '690'))
    application_area = models.CharField(max_length=50, null=True)
    supply_voltage = models.CharField(max_length=10, null=True, choices=voltage)
    product_family = models.CharField(max_length=50, null=True)
    performance = models.CharField(max_length=50, null=True)

    def __str__(self):
        return f"Product application area: {self.application_area}" \
               f", product supply voltage: {self.supply_voltage}"


