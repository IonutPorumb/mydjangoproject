from django.contrib import admin
from django.contrib.auth.models import Permission
# se adauga pentru a avea acces in admin si Permission este modelul
# Ne inregistram modelul in admission panel
# Cand trecem modelul Permission nu mai trebuie sa le cream noi create View etc. Cine vrea sa le creeze trebuie sa aiba coont de user
# Putem intra in admin.pannel din cadrul student si trecem numele modelului Student
admin.site.register(Permission)
