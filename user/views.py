from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic import CreateView

from user.forms import UserForms


class UserCreateView(CreateView):
    template_name = 'user/create_user.html'
    model = User
    form_class = UserForms
    success_url = reverse_lazy('index-home')
