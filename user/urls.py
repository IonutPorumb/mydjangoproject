from django.urls import path

from user import views

urlpatterns = [
    # (parametrul accesat de brovser, clasa accesata de noi din views, numele alocat de noi)
    path('create-user/', views.UserCreateView.as_view(), name='create_user'),

]
