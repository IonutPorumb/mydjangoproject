from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import TextInput, Select

from user.models import ExtendUser


class UserForms(UserCreationForm):
    class Meta:
        model = ExtendUser

        # Nu suprascriem password 1 si password2 pentru a nu face proprietatile prevazute de django mai vulnerabile
        # din punct de vedere al securitatii

        fields = ['first_name', 'last_name', 'email', 'username', 'sex', 'age', 'date_of_birth']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please insert your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please insert your last name', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Please insert your email', 'class': 'form-control'}),
            'username': TextInput(attrs={'placeholder': 'Please insert your user name', 'class': 'form-control'}),
            'sex': Select(attrs={'class': 'form-control'}),
            'age': TextInput(attrs={'placeholder': 'Please insert the age', 'class': 'form-control'}),
            'date_of_birth': TextInput(attrs={'class': 'form-control', 'type': 'date'})
        }

    def __init__(self, *args, **kwargs):
        super(UserForms, self).__init__(*args, **kwargs)
        # pentru self.fields la [password 1] ii atribuim in widget.attrs valoarea FormControl
        # modificarile se aplica campului din DB de aia se pune fields.
        # La acest camp ii atribuim o clasa de CSS care sunt class si placeholder
        self.fields['password1'].widget.attrs['class'] = 'FormControl'
        self.fields['password1'].widget.attrs['placeholder'] = 'Please insert a password'
        self.fields['password2'].widget.attrs['class'] = 'FormControl'
        self.fields['password2'].widget.attrs['placeholder'] = 'Please confirm the password'
