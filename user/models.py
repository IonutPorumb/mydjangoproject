from django.contrib.auth.models import User
from django.db import models


class ExtendUser(User):
    sex_option = (('Male', 'male'), ('Female', 'female'), ('Not defined', 'not defined'))
    age = models.IntegerField(null=True)
    date_of_birth = models.DateField(null=True)
    sex = models.CharField(max_length=20, choices=sex_option)

    def __str__(self):
        return f"First name{self.first_name}, Last name:{self.last_name}, Age: {self.age}"
