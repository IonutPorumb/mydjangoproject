# Aplicatia home
# In urls vom include toate url-urile din cadrul aplicatiei
# views.py este ceea ce dorim sa avem noi in aceasta interfata
# In views.py vom gasi metodele si clasele
# Putem sa ne facem un Domeniu pe herocu sau cloud platform sau abs de la amazon.
from django.urls import path

from home import views

urlpatterns = [
    # daca se lasa stringul gol atunci se foloseste calea root.
    # Altfel trebuie sa avem modelul din string aplicat pe calea de acces
    # path('numele care vreau eu sa il am eu in path',
    # numele metodei accesate,
    # numele care il aloc pentru path
    # Nu avem voie sa avem doua nume la fel cu metode diferite
    path('index/', views.index, name='index-home'),
    # path('home/', views.home, name='your_name')
    path('all-students/', views.student, name='students'),
    path('', views.student, name='students'),
    path('all-authors/', views.author, name='authors'),

]