import datetime

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

# Putem defini metode si clase
from django.shortcuts import render


def index(request):
    return HttpResponse('Hello world!')


# def home(request):
#     return HttpResponse('Hello second world!')
# daca metodele vor avea un url se va prevedea ca prametru 'request'

@login_required
def student(request):
    context = {
        'all_students': [
            {
                'first_name': 'Anca',
                'last_name': 'Roman',
                'age': '34'

            },
            {
                'first_name': 'Ionut',
                'last_name': 'Porumb',
                'age': '17'

            }
        ]
    }
    # return render(request, 'base.html', context)
    return render(request, 'home/student.html', context)


def author(request):
    context_author = {
        'all_authors': [
            {
                'author_name': 'Ion Creanga',
                'date': datetime.datetime.today(),
                'image': 'https://upload.wikimedia.org/wikipedia/commons/2/27/Ion_Creanga-Foto03.jpg',
                'now': datetime.datetime.now().month

            },
            {
                'author_name': 'Mihai Eminescu',
                'date': datetime.datetime.today() - datetime.timedelta(days=30),
                'image': 'https://upload.wikimedia.org/wikipedia/commons/d/dc/Eminescu.jpg',
                'now': datetime.datetime.now().month
            }

        ]
    }
    return render(request, 'home/authors.html', context_author)
