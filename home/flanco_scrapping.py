import bs4
import requests
import xlsxwriter

url = 'https://www.flanco.ro/laptop-it-tablete/laptop/windows-laptop.html'
result = requests.get(url)
soup = bs4.BeautifulSoup(result.text, 'lxml')
cases = soup.find_all('li', class_='item product product-item produs')
context = {'data': []}
for case in cases:
    data = {}
    product_name = case.find('a', class_='product-item-link')
    product_price = case.find('span', class_='price')
    if not product_name:
        data['product_name'] = 'No data available'
    else:
        data['product_name'] = product_name.text
    if not product_price:
        data['product_price'] = 'No data available'
    else:
        data['product_price'] = product_price.text
    context['data'].append(data)
workbook = xlsxwriter.Workbook('Laptop_list_flanco.xlsx')
worksheet = workbook.add_worksheet('Laptops')

row = 0
column = 0

for date in context['data']:
    worksheet.write(row, column, date['product_name'])
    worksheet.write(row, column + 1, date['product_price'])
    row += 1

workbook.close()
