import requests
import bs4
import xlsxwriter
url = 'https://www.emag.ro/placi_video/stoc/c?ref=lst_leftbar_6407_stock'
result = requests.get(url)
soup = bs4.BeautifulSoup(result.text, 'lxml')
cases = soup.find_all('div', class_='card')
context = {'data': []}
for case in cases:
    data = {}
    product_name = case.find('a', class_='product-title js-product-url')
    product_price = case.find('p', class_='product-new-price')
    if not product_name:
        data['product_name'] = 'No data available'
    else:
        data['product_name'] = product_name.text
    if not product_price:
        data['product_price'] = 'No data available'
    else:
        data['product_price'] = product_price.text
    context['data'].append(data)
#     Se creaza documentul excel
workbook = xlsxwriter.Workbook('Video_cards_list.xlsx')
#     Se creaza sheet-ul in care vor fi adaugate datele
worksheet = workbook.add_worksheet('Video cards')
# Specificam randul si coloana de la care incepem scrierea de date
row = 0
column = 0
# print(context['data'])
for date in context['data']:
    worksheet.write(row, column, date['product_name'])
    worksheet.write(row, column + 1, date['product_price'])
    row += 1

workbook.close()





# print(context)
    # if product_price is None:
    #     print('not available')
    # else:
    #     print(product_price.text)
    # sau print(product_price.string)
