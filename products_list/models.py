from django.db import models

from product_category.models import Category


class ProductList(models.Model):
    product_code_no = models.CharField(max_length=50, null=True)
    product_description = models.TextField(max_length=500)
    product_producer = models.CharField(max_length=50, null=True)
    product_size = models.CharField(max_length=30, null=True)
    product_price = models.FloatField(null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"Product code no.: {self.product_code_no}, " \
               f"Description: {self.product_description}" \
               f"Price : {self.product_price}"
