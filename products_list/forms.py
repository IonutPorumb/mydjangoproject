from django import forms
from django.forms import TextInput, Textarea, Select

from products_list.models import ProductList


class ProductListForm(forms.ModelForm):
    class Meta:
        model = ProductList
        fields = ['product_code_no', 'product_description', 'product_producer', 'product_size',
                  'product_price', 'category']
        widgets = {
            'product_code_no': TextInput(
                attrs={'placeholder': 'Please insert a product code no.', 'class': 'form-control'}),
            'product_description': Textarea(
                attrs={'placeholder': 'Please insert a product description', 'class': 'form-control'}),
            'product_producer': TextInput(
                attrs={'placeholder': 'Please insert a product producer name', 'class': 'form-control'}),
            'product_size': TextInput(attrs={'placeholder': 'Please insert the product size', 'class': 'form-control'}),
            'product_price': TextInput(
                attrs={'placeholder': 'Please insert the product price', 'class': 'form-control'}),
            'category': Select(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(ProductListForm, self).__init__(*args, **kwargs)
