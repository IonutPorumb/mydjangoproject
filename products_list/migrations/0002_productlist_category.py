# Generated by Django 3.2.5 on 2021-08-14 04:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('product_category', '0002_alter_category_supply_voltage'),
        ('products_list', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='productlist',
            name='category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='product_category.category'),
        ),
    ]
