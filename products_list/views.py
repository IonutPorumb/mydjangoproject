from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView

from products_list.forms import ProductListForm

from products_list.models import ProductList


# Create products list page
class ProductListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'product_list/product_list_page.html'
    model = ProductList
    context_object_name = 'all_products'


# Create product detail view
class ProductDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'product_list/detail_product.html'
    model = ProductList
    context_object_name = 'product_detail'


# Create products in DB
class ProductListCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'product_list/create_product_list.html'
    form_class = ProductListForm
    success_url = reverse_lazy('create_product_list')


# Edit created products
class ProductsListUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'product_list/update_product_list.html'
    model = ProductList
    form_class = ProductListForm
    success_url = reverse_lazy('show_product_list')


# Delete existing products
class ProductListDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'product_list/delete_product.html'
    model = ProductList
    success_url = reverse_lazy('show_product_list')
    context_object_name = 'productList'
