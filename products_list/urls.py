from django.urls import path

from products_list import views

urlpatterns = [
    path('create-product-list/', views.ProductListCreateView.as_view(), name='create_product_list'),
    path('product-list-view/', views.ProductListView.as_view(), name='show_product_list'),
    path('product-detail-view/<int:pk>/', views.ProductDetailView.as_view(), name='detail_product_list'),
    path('edit-products-in-list/<int:pk>/', views.ProductsListUpdateView.as_view(), name='edit_products_in_list'),
    path('delete-product-in-list/<int:pk>/', views.ProductListDeleteView.as_view(), name='delete_product'),
]
