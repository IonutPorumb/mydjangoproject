from professor.models import Professor
from student.models import Student


def get_all_students(request):
    all_students = Student.objects.all()
    return {'context_students': all_students}


def get_all_professors(request):
    all_professors = Professor.objects.all()
    return {'context_professors': all_professors}