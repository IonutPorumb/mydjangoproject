
from django import forms
from django.forms import TextInput, Select, Textarea, CheckboxInput

from professor.models import Professor


class ProfessorForm(forms.ModelForm):
    class Meta:
        model = Professor
        fields = ['first_name', 'last_name', 'sex', 'description', 'subject', 'active', 'age', 'date_of_birth']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please insert the first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please insert the last name', 'class': 'form-control'}),
            'sex': Select(attrs={'class': 'form-control'}),
            'description': Textarea(attrs={'placeholder': 'Please insert your description', 'class': 'form-control'}),
            'subject': Select(attrs={'class': 'form-control'}),
            'active': CheckboxInput(attrs={'class': 'checkbox'}),
            'age': TextInput(attrs={'placeholder': 'Please insert the age', 'class': 'form-control'}),
            'date_of_birth': TextInput(attrs={'class': 'form-control', 'type': 'date'})

        }

    def __init__(self, *args, **kwargs):
        super(ProfessorForm, self).__init__(*args, **kwargs)

