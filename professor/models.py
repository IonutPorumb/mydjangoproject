from django.db import models


class Professor(models.Model):
    subject_option = (('Mathematics', 'mathematics'), ('Physics', 'physics'), ('Geography', 'geography'))
    sex_option = (('Male', 'male'), ('Female', 'female'), ('Not defined', 'not defined'))
    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=50)
    age = models.IntegerField(null=True)
    date_of_birth = models.DateField(null=True)
    active = models.BooleanField(default=False)
    sex = models.CharField(max_length=20, choices=sex_option)
    description = models.TextField(max_length=500, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)
    subject = models.CharField(max_length=40, choices=subject_option)
    # # Cream un foreign key catre tabelul studenti
    # student = models.ForeignKey(Student, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"First name: {self.first_name}, Last name: {self.last_name}, Creation date: {self.created_at}"
