# Generated by Django 3.2.5 on 2021-08-05 18:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Professor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50, null=True)),
                ('last_name', models.CharField(max_length=50)),
                ('age', models.IntegerField(null=True)),
                ('date_of_birth', models.DateField(null=True)),
                ('active', models.BooleanField(default=False)),
                ('sex', models.CharField(choices=[('Male', 'male'), ('Female', 'female'), ('Not defined', 'not defined')], max_length=20)),
                ('description', models.TextField(max_length=500, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('subject', models.CharField(choices=[('Mathematics', 'mathematics'), ('Physics', 'physics'), ('Geography', 'geography')], max_length=40)),
            ],
        ),
    ]
