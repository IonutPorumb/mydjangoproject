from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from professor.forms import ProfessorForm
from professor.models import Professor


class ProfessorCreateView(LoginRequiredMixin, CreateView):
    template_name = 'professor/create_professor.html'
    form_class = ProfessorForm
    success_url = reverse_lazy('index-home')


class ProfessorListView(LoginRequiredMixin, ListView):
    template_name = 'professor/list_professor.html'
    model = Professor
    context_object_name = 'all_professors'


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'professor/update_professor.html'
    model = Professor
    form_class = ProfessorForm
    success_url = reverse_lazy('create-list-of-professors')

