from django.urls import path

from professor import views

urlpatterns = [
    path('create-professor/', views.ProfessorCreateView.as_view(), name='create-professor'),
    path('list-of-professors/', views.ProfessorListView.as_view(), name='create-list-of-professors'),
    path('update-professor/<int:pk>', views.StudentUpdateView.as_view(), name='update_professor')

]