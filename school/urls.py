from django.urls import path

from school import views

urlpatterns = [
    path('create-school/', views.SchoolCreateView.as_view(), name='create-school')
]
