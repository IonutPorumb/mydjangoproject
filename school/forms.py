from django import forms
from django.forms import TextInput, Select

from school.models import School


class SchoolForm(forms.ModelForm):
    class Meta:
        model = School
        fields = ['school_name', 'school_city', 'school_country', 'school_foundation', 'school_profile',
                  'school_address', 'number_of_students', 'school_standard']
        widgets = {
            'school_name': TextInput(attrs={'placeholder': 'Please insert the name of the school',
                                            'class': 'form-control'}),
            'school_city': TextInput(attrs={'placeholder': 'Please insert the city of the school',
                                            'class': 'form-control'}),
            'school_country': TextInput(attrs={'placeholder': 'Please insert the country of the school',
                                               'class': 'form-control'}),
            'school_foundation': TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            'school_profile': Select(attrs={'class': 'form-control'}),
            'school_address': TextInput(attrs={'placeholder': 'Please insert the school address',
                                               'class': 'form-control'}),
            'number_of_students': TextInput(attrs={'placeholder': 'Please insert the number of students',
                                                   'class': 'form-control'}),
            'school_standard': Select(attrs={'class': 'form-control'})
        }

        def __init__(self, *args, **kwargs):
            super(SchoolForm, self).__init__(*args, **kwargs)
