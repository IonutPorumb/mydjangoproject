from django.db import models


class School(models.Model):
    standard_option = (('High', 'high'), ('Medium', 'medium'), ('Low', 'low'))
    profile_options = (('Technic', 'technic'), ('Theoretical', 'theoretical'), ('Humanities', 'humanities'))
    school_name = models.CharField(max_length=50, null=True)
    school_city = models.CharField(max_length=50, null=True)
    school_country = models.CharField(max_length=50, null=True)
    school_profile = models.CharField(max_length=30, choices=profile_options)
    school_foundation = models.DateField(null=True)
    school_address = models.CharField(max_length=100, null=True)
    number_of_students = models.IntegerField(null=True)
    school_standard = models.CharField(max_length=30, choices=standard_option)

    def __str__(self):
        return "School name: ", self.school_name, "School city: ", self.school_city, \
               "School standards: ", self.school_standard
