
from django.urls import reverse_lazy
from django.views.generic import CreateView

from school.forms import SchoolForm


class SchoolCreateView(CreateView):
    template_name = 'school/create_school.html'
    form_class = SchoolForm
    success_url = reverse_lazy('index-home')
