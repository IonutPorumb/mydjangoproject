from django import forms
from django.forms import TextInput, Select, Textarea, CheckboxInput

from student.models import Student


# Clasa este bazata pe un formulat ModelForm
class StudentForm(forms.ModelForm):
    # Pe baza class Meta vom obtine un un formular bazat pe model.
    class Meta:
        # specificam modelul de customizare a formularului
        model = Student
        #         specificam fieldurile care le vom avea in formular se copiaza din views.py
        fields = ['first_name', 'last_name', 'sex', 'description', 'active', 'age', 'date_of_birth', 'professor']
        # ne customizam fiecare camp prin adaugare de place holder si adaugare de css
        # attrs -> pentru adaugarea de atribute
        # form-control este o clasa css din Bootstrap pentru stilizare
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please insert the first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please insert the last name', 'class': 'form-control'}),
            'sex': Select(attrs={'class': 'form-control'}),
            'description': Textarea(attrs={'placeholder': 'Please insert your description', 'class': 'form-control'}),
            'active': CheckboxInput(attrs={'class': 'checkbox'}),
            'age': TextInput(attrs={'placeholder': 'Please insert the age', 'class': 'form-control'}),
            'date_of_birth': TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            #  Se creaza si professor pentru a face o stilizare mai frumoasa
            'professor': Select(attrs={'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
