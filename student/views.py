# Create your views here.

from io import BytesIO
from xhtml2pdf import pisa
from django.http import HttpResponse


from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
# The classes are named using the name and at the end CreateView
from student.forms import StudentForm
from student.models import Student


# Insert registrations in DB
class StudentCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    # we define the properties
    template_name = 'student/create_student.html'
    form_class = StudentForm
    permission_required = 'student.add_student'
    # model = Student
    # we introduce the fields that we need to have in the view
    # fields = '__all__'
    # we create a list with the fields that we need to have in the view
    # fields = ['first_name', 'last_name', 'sex', 'description', 'age', 'date_of_birth']
    # success_url = '/' => redirectionare in root dupa salverea datelor in DB se da calea din url
    success_url = reverse_lazy('create-list-of-students')


# Show date DB
# Se adauga LoginREquiredMixin pentru a limita accesul pe baza de login
#  Ii spunemm clasei ca mai intai e necesara autentificarea si apoi se face afisarea.
class StudentListView(LoginRequiredMixin, ListView):
    template_name = 'student/list_students.html'
    model = Student
    permission_required = 'student.view_list of student'
    # Facem referire la all_students din home views
    context_object_name = "all_students"


# Update date DB
class StudentUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'student/update_student.html'
    model = Student
    permission_required = 'student.change_student'
    form_class = StudentForm
    success_url = reverse_lazy('create-list-of-students')


# Delete a registration from DB
class StudentDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'student/delete_student.html'
    model = Student
    permission_required = 'student.delete_student'
    success_url = reverse_lazy('create-list-of-students')
    context_object_name = 'student'


# Create Details view
class StudentDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'student/detail_student.html'
    model = Student
    permission_required = 'student.view_student'
    context_object_name = 'students'

# template_src este template -ul care vrem sa il stilizam
# context_dict se refera la acel context la care noi trimitem datele
# UTF-8 se face decodificarea
# daca nu sunt erori ne returneaza intr-un format pdf datele
# BytesIO ne permite sa realizam operatiuni de intrare si iesire in fisierul pdf
# pisa este o librarie de la Django care ia documentul template si introduce datele si le codifica
# pisa este un convertor din html in pdf si este o librarie pentru a face acest lucru
# content este o sintaxa prin care denumim fisierul pdf
# content disposition verifica daca nu a fost generata aceasta denumire se realizeaza raspunsul.


def render_to_pdf(template_src, context_dict):
        template = get_template(template_src)
        html = template.render(context_dict)
        result = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
        if not pdf.err:
            return HttpResponse(result.getvalue(), content_type='application/pdf')
        return None


def generate_pdf(request):
    all_students = Student.objects.all()  # interogam toti studentii din baza de date

    context = {
        'students': all_students
    }
    # Se creaza o functie generica care se poate schimba
    pdf_file = render_to_pdf('student/student_pdf.html', context)
    if pdf_file:
        response = HttpResponse(pdf_file, content_type='application/pdf')
    filename = 'Report with list of students'
    content = "inline; filename=%s" % filename
    response['Content-Disposition'] = content
    return response

    # Daca dorim sa facem o filtrare pe lista de studenti se creaza un primary key


def generate_pdf_per_student(request, pk):
    # get se foloseste cand vrem sa returnam o singura valoare iar filter se foloselste cand vrem sa
    # returnam mai multe valori pe baza unei filtrari
    # la get nu mai trebuie sa facem un for pentru ca ne returneaza o singura valoare
    # la filter este necesara parcurgerea listei cu un for
    student = Student.objects.get(id=pk)
    context = {
        'details_student': student
    }
    pdf_file = render_to_pdf('student/one_student.html', context)
    if pdf_file:
        response = HttpResponse(pdf_file, content_type='application/pdf')
    filename = f'Details about the student {student.first_name} {student.last_name}'
    content = 'inline; filename=%s' % filename
    response['Content-Disposition'] = content
    return response
