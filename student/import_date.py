import datetime
import sqlite3
import xlrd

all_students = xlrd.open_workbook('Date_Student_2.xls')  # Se defineste calea catre fisierul excel
sheet = all_students.sheet_by_name('Sheet1')

data_base = sqlite3.connect('../db.sqlite3')  # conectarea aplicatiei la DB
cursor = data_base.cursor()

for row in range(1, sheet.nrows):
    first_name = sheet.cell(row, 0).value
    last_name = sheet.cell(row, 1).value
    age = int(sheet.cell(row, 2).value)
    get_value_birth_date = sheet.cell(row, 3).value
    birth_date = datetime.datetime(*xlrd.xldate_as_tuple(get_value_birth_date, all_students.datemode)).date()
    active = int(sheet.cell(row, 4).value)
    sex = sheet.cell(row, 5).value
    description = sheet.cell(row, 6).value
    professor = int(sheet.cell(row, 7).value)
    created_at = datetime.datetime.now()
    updated_at = datetime.datetime.now()
    # Comanda de update  a randurilor in functie de id
    new_first_name = sheet.cell(row, 0).value
    id = int(sheet.cell(row, 8).value)
    print(new_first_name, "si", id)
    # Verificarea datelor importate din xls
    # print(first_name, " ", last_name, " ", age, " ", birth_date, " ", active, " ",
    #       sex, " ", description, " ", professor)

    # Comanda de insert a datelor in table excel
    # query = f"INSERT INTO student_student(first_name, last_name, age, date_of_birth, active, sex, description, " \
    #         f"professor_id, created_at, updated_at) VALUES ('{first_name}', '{last_name}', '{age}', '{birth_date}', '{active}', '{sex}', " \
    #         f"'{description}', '{professor}', '{created_at}', '{updated_at}')"

    # UPDATE TABLE_NAME SET COLUMN1 = VALUE1 WHERE ID=ID
    query = f"UPDATE student_student SET first_name='{new_first_name}' where id = {id}"

    cursor.execute(query)
cursor.close()
data_base.commit()
data_base.close()
