from django.db import models

# We define the table structure
from professor.models import Professor


class Student(models.Model):
    # POzitia 1 este ce se afisaza in DB si a doua pozitie este ce se afisaza in front end
    sex_option = (('Male', 'male'), ('Female', 'female'), ('Not defined', 'not defined'))
    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=100)
    age = models.IntegerField(null=True)
    date_of_birth = models.DateField(null=True)
    active = models.BooleanField(default=False)
    sex = models.CharField(max_length=20, choices=sex_option)
    description = models.TextField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # Cream un foreign key catre tabelul profesor
    professor = models.ForeignKey(Professor, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"{self.first_name}, {self.last_name}, {self.created_at}"
