from django.urls import path

from student import views

urlpatterns = [
    path('create-student/', views.StudentCreateView.as_view(), name='create-student'),
    path('list-of-students/', views.StudentListView.as_view(), name='create-list-of-students'),
    # <int:pk> -> inseamna integer si primary key
    path('update-student/<int:pk>/', views.StudentUpdateView.as_view(), name='update_student'),
    path('delete_student/<int:pk>/', views.StudentDeleteView.as_view(), name='delete_student'),
    path('detail_student/<int:pk>/', views.StudentDetailView.as_view(), name='detail_student'),
    path('generate_pdf/', views.generate_pdf, name='generate_pdf'),
    path('generate_pdf_student/<int:pk>/', views.generate_pdf_per_student, name='generate-pdf-student'),

]
